package com.project.mq.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class MyProducer {
    private final static String QUEUE_NAME = "ORIGIN_QUEUE1";
    private final static String EXCHANGE_NAME = "SIMPLE_EXCHANGE1";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        // 连接IP
        factory.setHost("192.168.0.105");
        // 连接端口
        factory.setPort(5672);
        // 虚拟机
        factory.setVirtualHost("/");
        // 用户
        factory.setUsername("guest");
        factory.setPassword("guest");

        // 建立连接
        Connection conn=factory.newConnection();
        // 创建消息通道
        Channel channel=conn.createChannel();
        // 发送消息
        String msg="Hello RabbitMQ!";

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.basicPublish("", QUEUE_NAME, null, msg.getBytes());

        channel.close();
        conn.close();

    }
}
