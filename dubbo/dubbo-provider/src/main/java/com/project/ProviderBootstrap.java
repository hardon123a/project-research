package com.project;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDubboConfiguration
@SpringBootApplication
public class ProviderBootstrap {
    public static void main(String[] args) {
        try {
            SpringApplication.run(ProviderBootstrap.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
