package com.hjp.iplimiter.controller;

import com.hjp.iplimiter.annotation.IpLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IpController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IpController.class);
    private static final String MESSAGE = "请求失败,你的IP访问太频繁";

    @ResponseBody
    @RequestMapping("iplimiter")
    @IpLimiter(ipAddress = "192.168.0.104",limit = 5,time = 10, message = MESSAGE)
    public String sendPayment() {
        return "请求成功";
    }
}
