package com.research.deep.controller;

import com.research.deep.config.Config2;
import com.research.deep.config.Config4;
import com.research.deep.config.ConfigurationBindingDemo;
import com.research.deep.config.ConfigurationBindingDemo4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigurationBindingDemo1Controller {

    /*@Autowired
    private ConfigurationBindingDemo configurationBindingDemo;

    @Autowired
    private Config2 config2;*/

    @Autowired
    private ConfigurationBindingDemo4 config4;

    @RequestMapping(value = "/demo1",method = RequestMethod.GET)
    public String getProperties(){
        return config4.getEmail();
    }
}
