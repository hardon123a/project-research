package com.research.deep.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(ConfigurationBindingDemo5.class)
public class DemoAutoConfiguration {
}
