package com.research.deep.config;

/**
 * 方式2：@Bean+@ConfigurationProperties
 * 如果同一个类需要注册成多个bean（即多个DataObject实例），可以采用这种方式
 */
public class ConfigurationBindingDemo2 {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
