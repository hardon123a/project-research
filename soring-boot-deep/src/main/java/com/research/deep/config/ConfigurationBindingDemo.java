package com.research.deep.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 方式1：@component+@ConfigurationProperties
 */
@Component
@ConfigurationProperties(prefix = "demo")
public class ConfigurationBindingDemo {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
