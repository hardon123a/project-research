package com.research.deep.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @EnableConfigurationProperties
 * 将@EnableConfigurationProperties标在一个配置类上，指定多个标有 @ConfigurationProperties的类
 */
@Configuration
@EnableConfigurationProperties(ConfigurationBindingDemo4.class)
public class Config4 {
}
