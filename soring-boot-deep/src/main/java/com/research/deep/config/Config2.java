package com.research.deep.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config2 {

    @Bean
    @ConfigurationProperties(prefix = "demo1")
    public ConfigurationBindingDemo2 demo1() {
        return new ConfigurationBindingDemo2();
    }

    @Bean
    @ConfigurationProperties(prefix = "demo2")
    public ConfigurationBindingDemo2 demo2() {
        return new ConfigurationBindingDemo2();
    }
}
