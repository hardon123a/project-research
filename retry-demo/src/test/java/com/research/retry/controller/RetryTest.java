package com.research.retry.controller;

import com.research.retry.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetryTest.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RetryTest {

    @Autowired
    private PayService payService;

    @Test
    public void minGoodsNum()  {
        payService.method(10);
    }

    @Test
    public void test(){
        System.out.println("test!");
    }
}

