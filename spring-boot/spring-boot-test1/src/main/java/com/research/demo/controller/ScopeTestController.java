package com.research.demo.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * https://fking.blog.csdn.net/article/details/108529996?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-8.baidujs&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-8.baidujs
 https://blog.csdn.net/csj50/article/details/83016508?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-3.baidujs&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-3.baidujs
 https://blog.csdn.net/xiaoxiaole0313/article/details/109699164?utm_medium=distribute.pc_relevant.none-task-blog-baidujs_title-0&spm=1001.2101.3001.4242

 */
@Controller
//@Scope("prototype")
public class ScopeTestController {
    private int num = 0;
    private final ThreadLocal<Integer> uniqueNum=new ThreadLocal<Integer>(){
        @Override
        protected Integer initialValue() {
            return num;
        }
    };

    @RequestMapping("/testScope")
    public void testScope() {
        System.out.println(++num);
    }

    @RequestMapping("/testScope2")
    public void testScope2() {
        System.out.println(++num);
    }

    @RequestMapping("/testScope3")
    public void testScope3() {

        int uum=uniqueNum.get();
        uniqueNum.set(++uum);

        System.out.println(uniqueNum.get());
    }
}
