package com.research.file.controller;

import com.research.file.config.RemoteProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableConfigurationProperties(RemoteProperties.class)
@RestController
public class PropertiesController {

    @Autowired
    RemoteProperties remoteProperties;

    @GetMapping
    @RequestMapping("/testpro")
    public void test(){
        String str = remoteProperties.getUploadFilesUrl();
        System.out.println(str);
    }
}


