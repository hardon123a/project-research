package com.research.file.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class OrderService {

    public CompletableFuture<String> todayOrderCount() {
        return CompletableFuture.supplyAsync(() -> this.getTodayOrderCount());
    }

    public CompletableFuture<String> todayTurnover() {
        return CompletableFuture.supplyAsync(() -> this.getTodayTurnover());
    }

    public CompletableFuture<String> totalTurnover() {
        return CompletableFuture.supplyAsync(() -> this.getTotalTurnover());
    }
    /**
     * 查询今日订单数
     * @return
     */
    private String getTodayOrderCount(){
        System.out.println(">>>>>>> 查询今日订单数:" + Thread.currentThread().getName());

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "50";
    }

    /**
     * 今日交易额
     * @return
     */
    private String getTodayTurnover(){

        System.out.println(">>>>>>> 查询今日交易额:" + Thread.currentThread().getName());

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "200";
    }

    /**
     * 查询今日总交易额
     * @return
     */
    private String getTotalTurnover() {

        System.out.println(">>>>>>> 查询总交易额:" + Thread.currentThread().getName());

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "800";
    }
}
