package com.research.file.controller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("file")
@Slf4j
public class FileController {

    @Value("${file.upload.url}")
    private String uploadFilePath;

    private String downloadFilePath;

    private String deleteFilePath;

    @PostMapping
    @RequestMapping("/upload")
    public String upload(@RequestParam("files") MultipartFile files[]) {

        JSONObject object = new JSONObject();

        for (int i = 0; i < files.length; i++) {

            String fileName = files[i].getOriginalFilename();//文件名
            File dest = new File(uploadFilePath+'/'+fileName);
            if(dest.getParentFile().exists()){
                dest.getParentFile().mkdirs();
            }
            try {
                files[i].transferTo(dest);
            } catch (IOException e) {
                log.error("{}",e);
                object.put("success", 2);
                object.put("result", "程序错");
            }
        }

        object.put("success", 1);
        object.put("result", "文件上传成功");

        return object.toString();
    }

    /*@RequestMapping("/download")
    public String loadload(){

    }

    @Scheduled(cron = "0 0 3 * * ?")
    public void deleteFiles(){
        deleteFile(new File(deleteFilePath));
    }*/


}
