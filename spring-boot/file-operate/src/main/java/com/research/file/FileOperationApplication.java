package com.research.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
//@EnableScheduling
public class FileOperationApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileOperationApplication.class, args);
    }

}
