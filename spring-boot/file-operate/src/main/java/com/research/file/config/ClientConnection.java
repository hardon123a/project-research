package com.research.file.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(RemoteProperties.class)
public class ClientConnection {

    @Autowired
    RemoteProperties remoteProperties;

    public void createClient() throws Exception {
        System.out.println(remoteProperties.getUploadFilesUrl());
    }

}
