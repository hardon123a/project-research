package com.project.spring.dao.dao.impl;

import com.project.spring.dao.dao.IAccountDao;

/**
 * 账号的持久层实现
 */
public class AccountDaoImpl implements IAccountDao {
    @Override
    public void saveAccount() {
        System.out.println("保存了账号");
    }
}
