package com.project.spring.dao.service;

/**
 * 账号业务层的接口
 */
public interface IAccountService {

    /**
     * 模拟保存账号
     */
    void saveAccount();
}
