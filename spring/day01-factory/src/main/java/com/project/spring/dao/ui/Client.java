package com.project.spring.dao.ui;

import com.project.spring.dao.factory.BeanFactory;
import com.project.spring.dao.service.IAccountService;

/**
 * 模拟一个表现层，调用业务
 */
public class Client {

    public static void main(String[] args) {
        //IAccountService as=new AccountServiceImpl();
        IAccountService as = (IAccountService) BeanFactory.getBean("accountService");
        for (int i = 0; i < 5; i++) {
            System.out.println(as);
            as.saveAccount();
        }
    }
}
