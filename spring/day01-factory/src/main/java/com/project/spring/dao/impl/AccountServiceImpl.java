package com.project.spring.dao.impl;

import com.project.spring.dao.dao.IAccountDao;
import com.project.spring.dao.factory.BeanFactory;
import com.project.spring.dao.service.IAccountService;

/**
 * 业务层的实现类
 */
public class AccountServiceImpl implements IAccountService {

    //private IAccountDao accountDao=new AccountDaoImpl();
    private IAccountDao accountDao = (IAccountDao) BeanFactory.getBean("accountDao");

    @Override
    public void saveAccount() {
        accountDao.saveAccount();
    }
}
