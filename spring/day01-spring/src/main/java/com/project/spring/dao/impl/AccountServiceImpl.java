package com.project.spring.dao.impl;

import com.project.spring.dao.dao.IAccountDao;
import com.project.spring.dao.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 业务层的实现类
 */
public class AccountServiceImpl implements IAccountService {

    private IAccountDao accountDao;

    public AccountServiceImpl() {
        System.out.println("对象创建了");
    }

    public void setAccountDao(IAccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public void saveAccount() {
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean.xml");
        accountDao = ac.getBean("accountDao", IAccountDao.class);
        accountDao.saveAccount();
    }
}
