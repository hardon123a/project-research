package com.project.spring.service.impl;

import com.project.spring.dao.IAccountDao;
import com.project.spring.service.IAccountService;

import java.util.Date;

public class AccountServiceImpl implements IAccountService {

    private String name;
    private Integer age;
    private Date birthday;

    private IAccountDao accountDao;

    public AccountServiceImpl(String name, Integer age, Date birthday, IAccountDao accountDao) {
        this.name = name;
        this.age = age;
        this.birthday = birthday;
        this.accountDao = accountDao;
    }

    @Override
    public void saveAccount() {

        System.out.println("service中的saveAccount方法执行了。。。" + name + "," + age + "," + birthday);
        accountDao.saveAccount();
    }

}
