package com.hjp.dstributedlock.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.hjp.dstributedlock.mapper")
public class MyBatisConfig {

}
