package com.hjp.mylock.util;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

public class UnsafeInstance {

    //Unsafe可以绕过虚拟机，直接操作底层的内存，只能通过bootstrap加载器加载，只能通过反射获取
    public static Unsafe reflectGetUnsafe(){
        try {
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            return (Unsafe) field.get(null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
