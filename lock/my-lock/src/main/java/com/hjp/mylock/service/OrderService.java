package com.hjp.mylock.service;

import com.hjp.mylock.lock.AqsLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class OrderService {

    @Autowired
    JdbcTemplate template;

    AqsLock lock=new AqsLock();
    /*
     * 减库存
     * */
    public String reduceInventory() {
        lock.lock();

        List<Map<String, Object>> result = template.queryForList("select stock from shop_order where id=1");
        Integer store = (Integer)result.get(0).get("stock");
        if (result==null||result.isEmpty()||store<=0){
            System.out.println("下单失败，没有库存");
            lock.unlock();
            return "下单失败，没有库存";
        }
        --store;
        template.update("update shop_order set stock=? where id=1",store);
        System.out.println("下单成功，当前剩余库存"+store);
        lock.unlock();

        return "下单成功，当前剩余库存"+store;
    }
}
