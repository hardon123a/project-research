package com.hjp.mylock.controller;

import com.hjp.mylock.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    private OrderService service;

    @RequestMapping("/order")
    public String order(){
        return service.reduceInventory();
    }
}
