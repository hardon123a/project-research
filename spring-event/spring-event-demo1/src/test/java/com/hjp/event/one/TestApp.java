package com.hjp.event.one;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class TestApp {

    @Resource
    private IEventService eventService;

    @Test
    public void testEventPublish(){
        eventService.publisher(1L,"name");
    }
}
