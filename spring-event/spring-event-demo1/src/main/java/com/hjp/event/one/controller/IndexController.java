package com.hjp.event.one.controller;

import com.hjp.event.one.IEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @Autowired
    private IEventService eventService;

    @RequestMapping("/register")
    public String order(){
        //测试
        System.out.println("注册。。。");
        eventService.publisher(1l,"huang jianping");
        //其他信息
        return "OK";
    }
}
