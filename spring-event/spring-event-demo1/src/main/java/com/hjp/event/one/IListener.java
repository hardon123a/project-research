package com.hjp.event.one;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

//这里实现监听注意这里填写自己的事件类 如果不填写默认是监听applicationEvent类
//@Component
public class IListener implements ApplicationListener<IEvent> {

    @Override
    public void onApplicationEvent(IEvent event) {
        System.out.println(event.toString()+"监听到的事件");
    }
}
