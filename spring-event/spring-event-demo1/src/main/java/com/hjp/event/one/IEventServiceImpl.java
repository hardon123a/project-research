package com.hjp.event.one;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class IEventServiceImpl implements IEventService{

    @Resource
    private ApplicationContext context;

    @Override
    public void publisher(Long id, String msg) {

        //这里使用ApplicationContext 上下文应用对象发布时间消息
        context.publishEvent(new IEvent(context,id,msg));

    }
}
