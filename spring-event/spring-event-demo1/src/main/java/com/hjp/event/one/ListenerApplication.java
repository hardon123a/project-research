package com.hjp.event.one;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class ListenerApplication implements WebMvcConfigurer {

        public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(ListenerApplication.class, args);
        //context.addApplicationListener(new IListener());
        //context.publishEvent(new IEvent(context,1L,"测试"));
    }
}
