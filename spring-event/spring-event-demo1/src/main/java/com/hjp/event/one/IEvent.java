package com.hjp.event.one;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

/**
 * 事件类
 */
@Data
public class IEvent extends ApplicationEvent {

    private Long id;

    private String name;


    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public IEvent(Object source, Long id, String name) {
        super(source);
        this.id=id;
        this.name=name;
    }
}
