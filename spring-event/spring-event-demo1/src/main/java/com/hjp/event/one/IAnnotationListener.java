package com.hjp.event.one;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class IAnnotationListener {

    @EventListener
    public void  onApplicationEvent(IEvent event){
        System.out.println("后台正在生成名字用户ID为："+event.getId());
        System.out.println("后台正在生成名字："+event.getName());
    }

}
