package com.hjp.event.two.entity;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

//https://www.cnblogs.com/hurro/p/13549295.html
//@Component
public class MyBeanProcesssor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("调用postProcessBeforeInitialization（） 对"+beanName+"进行加工");
        if (bean instanceof A){
            A a = (A)bean;
            a.setName("008");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("调用postProcessAfterInitialization（） 再次获得"+beanName+"加工机会");
        return bean;
    }
}
