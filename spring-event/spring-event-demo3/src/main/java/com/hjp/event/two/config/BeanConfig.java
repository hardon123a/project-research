package com.hjp.event.two.config;

import com.hjp.event.two.entity.A;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    public A a(){
        A a = new A();
        a.setName("007");
        return a;
    }
}
