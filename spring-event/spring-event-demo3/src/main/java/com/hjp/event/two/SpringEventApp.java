package com.hjp.event.two;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * https://blog.csdn.net/flymoringbird/article/details/120481883
 * https://zhuanlan.zhihu.com/p/101128672
 */
@EnableAsync
@SpringBootApplication
public class SpringEventApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringEventApp.class, args);
    }
}
