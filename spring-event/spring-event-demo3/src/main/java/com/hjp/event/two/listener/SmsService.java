package com.hjp.event.two.listener;

import com.hjp.event.two.event.OrderSuccessEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 短信服务，监听OrderSuccessEvent
 */
@Component
public class SmsService implements ApplicationListener<OrderSuccessEvent> {
    @Override
    public void onApplicationEvent(OrderSuccessEvent event) {

        this.sendSms();
    }

    /**
     * 发送短信
     */
    @Async
    public void sendSms(){
        try {
            Thread.sleep(1000L * 3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread()+" 发送短信...");
    }
}
