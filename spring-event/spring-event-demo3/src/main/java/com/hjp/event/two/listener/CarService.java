package com.hjp.event.two.listener;

import com.hjp.event.two.event.OrderSuccessEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 物流服务
 */
@Service
public class CarService {

    //@Async
    @EventListener(OrderSuccessEvent.class)
    public void dispatch() {
        try {
            Thread.sleep(1000L * 6);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread()+" 开始配送...");
    }
}
