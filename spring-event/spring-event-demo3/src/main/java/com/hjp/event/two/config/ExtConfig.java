package com.hjp.event.two.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan("com.hjp.event.two")
@Configuration
public class ExtConfig {
}
