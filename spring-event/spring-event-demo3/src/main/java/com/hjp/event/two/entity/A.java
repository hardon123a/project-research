package com.hjp.event.two.entity;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class A implements BeanNameAware, BeanFactoryAware,InitializingBean, ApplicationContextAware,
        InstantiationAwareBeanPostProcessor
{

    public A() {
        System.out.println("A对象正在实例化 name="+ name);
    }
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println(">>>开始初始化 调用A setName()方法设置对象属性");
        this.name = name;
    }

    @Override
    //同一级的生命周期方法中最后一个被调用的,但是只会调用一次，之后在调用bean的setxx()方法更改属性时将不会再被被调用到
    public void afterPropertiesSet() throws Exception {
        System.out.println("调用afterPropertiesSet() A 对象的属性设置已经完成了 name=" + name);
    }

    @Override
    public void setBeanName(String s) {
        System.out.println("A实例属性设置完成，调用setBeanName() beanName=" + s);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("调用setApplicationContext  ApplicationContext的值为" + applicationContext.toString());
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("调用 setBeanFactory() 设置beanFactory实例到A对象中");
        //this.beanFactory = beanFactory;
        System.out.println("a >>>>>>>>"+ beanFactory.getBean("a").toString());
    }

}
