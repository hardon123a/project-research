package com.hjp.event.two.service.impl;

import com.hjp.event.two.event.OrderSuccessEvent;
import com.hjp.event.two.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void order() {
        // 下单成功
        System.out.println(Thread.currentThread()+" 下单成功...");
        applicationContext.publishEvent(new OrderSuccessEvent(this));
        // 发布通知
        /*new Thread(() ->{
            applicationContext.publishEvent(new OrderSuccessEvent(this));
        }).start();*/
        System.out.println(Thread.currentThread()+" main线程结束...");
        // 等SmsService结束
        /*try {
            Thread.sleep(1000L * 5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

    }
}
