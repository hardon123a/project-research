package com.hjp.event.two.entity;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

//@Component
public class MyInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {

    //在调用bean的构造方法实例化bean之前，都会先调用这个方法
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        System.out.println( beanName + "实例化之前 调用 postProcessBeforeInstantiation（）");
        return null;
    }

    //bean实例化完成之后，会调用这个方法：注意，根据我的实践来看，实例化bean的时候会把我们在配置类中指定的属性设置好之后才会调用这个方法，此时bean对象中的属性已经哟值了
    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        if (bean instanceof  A){
            A a = (A) bean;
            System.out.println("A实例对象此时的属性 name = " + a.getName());
        }
        System.out.println( beanName+ "对象实例化完成 调用postProcessAfterInstantiation 对象类型为：" + bean.getClass().getName() );
        return false;
    }
}
