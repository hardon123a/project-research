package com.hjp.event.two.service.impl;

import com.hjp.event.two.entity.UserDO;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @EventListener(classes={UserDO.class})
    public void listen(UserDO event){
        System.out.println("UserService。。监听到的事件："+event);
    }

}