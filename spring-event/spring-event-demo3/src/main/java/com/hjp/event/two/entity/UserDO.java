package com.hjp.event.two.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UserDO {

    private Integer id;

    private String name;
}
